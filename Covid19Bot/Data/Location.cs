using System;

namespace Covid19Bot.Data {
	public class Location {
		public int Id { get; set; }
		public string Country { get; set; }
		public string Country_code { get; set; }
		public int Country_population { get; set; }
		public string Province { get; set; }
		public DateTime Last_updated { get; set; }
		public Latest Latest { get; set; }
		public Coordinates Coordinates { get; set; }
	}
}