namespace Covid19Bot.Data {
	public class Coordinates {
		public string Latitude { get; set; }
		public string Longitude { get; set; }
	}
}