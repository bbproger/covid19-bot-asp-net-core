namespace Covid19Bot.Data {
	public class Latest {
		public long Confirmed { get; set; }
		public long Deaths { get; set; }
		public long Recovered { get; set; }
	}
}