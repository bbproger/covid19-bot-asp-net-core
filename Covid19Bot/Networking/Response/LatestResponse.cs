using Covid19Bot.Data;

namespace Covid19Bot.Networking.Response {
	public class LatestResponse {
		public Latest Latest { get; set; }
	}
}