using System.Collections.Generic;
using Covid19Bot.Data;

namespace Covid19Bot.Networking.Response {
	public class LocationsResponse {
		public Latest Latest { get; set; }
		public List<Location> Locations { get; set; }
	}
}