namespace Covid19Bot {
	public class BotConfiguration {
		public string Name { get; set; }
		public string Token { get; set; }
		public string WebHookUrl { get; set; }
		public string ExternalAPIUrl { get; set; }
	}
}