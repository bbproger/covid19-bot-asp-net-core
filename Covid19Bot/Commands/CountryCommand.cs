using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Covid19Bot.Networking.Response;
using Covid19Bot.Services;
using Telegram.Bot.Types;

namespace Covid19Bot.Commands {
	public class CountryCommand : Command {
		private string commandUrl;
		public override string Name => "/country";

		public CountryCommand(string apiUrl) {
			commandUrl = $"{apiUrl}/v2/locations?source=jhu";
		}

		public override async Task Execute(IBotService service, Message message) {
			var chatId = message.Chat.Id;
			var countryCode = message.Text.Split(" ").LastOrDefault();
			if (string.IsNullOrEmpty(countryCode)) {
				await service.Client.SendTextMessageAsync(chatId, "Wrong Format");
				return;
			}

			var data = await getLocation(countryCode);
			if (data?.Locations == null || data.Locations.Count == 0) {
				await service.Client.SendTextMessageAsync(chatId, "Country Not Found");
				return;
			}

			var replyMessage = 
				$"{data.Locations[0].Country}\n" +
				$"Confirmed: {data.Locations[0].Latest.Confirmed}\n" +
				$"Deaths: {data.Locations[0].Latest.Deaths}\n" +
				$"Recovered: {data.Locations[0].Latest.Recovered}";
			await service.Client.SendTextMessageAsync(chatId, replyMessage);
		}

		private async Task<LocationsResponse> getLocation(string countryCode) {
			var httpClient = HttpClientFactory.Create();
			commandUrl = $"{commandUrl}&country_code={countryCode}";
			var response = await httpClient.GetAsync(commandUrl);
			if (response.StatusCode != HttpStatusCode.OK) {
				return null;
			}

			var content = response.Content;
			var data = await content.ReadAsAsync<LocationsResponse>();
			return data;
		}
	}
}