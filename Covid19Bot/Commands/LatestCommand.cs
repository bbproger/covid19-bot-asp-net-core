using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Covid19Bot.Networking.Response;
using Covid19Bot.Services;
using Telegram.Bot.Types;

namespace Covid19Bot.Commands {
	public class LatestCommand : Command {
		private string commandUrl;
		public override string Name => "/latest";

		public LatestCommand(string apiUrl) {
			commandUrl = $"{apiUrl}/v2/latest?source=jhu";
		}

		public override async Task Execute(IBotService service, Message message) {
			var chatId = message.Chat.Id;
			var data = await getLatest();
			if (data == null) {
				await service.Client.SendTextMessageAsync(chatId, "Invalid Data");
				return;
			}

			var replyMessage =
				$"Confirmed: {data.Latest.Confirmed}\n" +
				$"Deaths: {data.Latest.Deaths}\n" +
				$"Recovered: {data.Latest.Recovered}";
			await service.Client.SendTextMessageAsync(chatId, replyMessage);
		}

		private async Task<LatestResponse> getLatest() {
			var httpClient = HttpClientFactory.Create();
			var response = await httpClient.GetAsync(commandUrl);
			if (response.StatusCode != HttpStatusCode.OK) {
				return null;
			}

			var content = response.Content;
			var data = await content.ReadAsAsync<LatestResponse>();
			return data;
		}
	}
}