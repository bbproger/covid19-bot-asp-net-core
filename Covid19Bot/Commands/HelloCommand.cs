using System.Threading.Tasks;
using Covid19Bot.Services;
using Telegram.Bot.Types;

namespace Covid19Bot.Commands {
	public class HelloCommand : Command {
		public override string Name => "/hello";

		public override async Task Execute(IBotService service, Message message) {
			var chatId = message.Chat.Id;
			var messageId = message.MessageId;
			await service.Client.SendTextMessageAsync(chatId, $"Hello: {message.Chat.Username}",
				replyToMessageId: messageId);
		}
	}
}