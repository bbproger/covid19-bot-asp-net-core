using System.Threading.Tasks;
using Covid19Bot.Services;
using Telegram.Bot.Types;

namespace Covid19Bot.Commands {
	public abstract class Command {
		public abstract string Name { get; }
		public abstract Task Execute(IBotService service, Message message);

		public virtual bool Contains(string name) {
			return !string.IsNullOrEmpty(name) && name.Contains(Name);
		}
	}
}