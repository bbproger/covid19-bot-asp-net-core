using System.Threading.Tasks;
using Covid19Bot.Services;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;

namespace Covid19Bot.Controllers {
	[ApiController]
	[Route("api/[controller]")]
	public class MessageController : ControllerBase {
		private IMessageService messageService;

		public MessageController(IMessageService messageService) {
			this.messageService = messageService;
		}

		[HttpGet]
		public IActionResult Get() {
			return Ok(new {status = "ok"});
		}

		public async Task<IActionResult> Post([FromBody] Update update) {
			await messageService.ProcessAsync(update);
			return Ok();
		}
	}
}