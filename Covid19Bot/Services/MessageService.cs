using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace Covid19Bot.Services {
	public class MessageService : IMessageService {
		private IBotService botService;
		private ICommandService commandService;

		public MessageService(IBotService botService, ICommandService commandService) {
			this.commandService = commandService;
			this.botService = botService;
		}

		public async Task ProcessAsync(Update update) {
			if (update.Type != UpdateType.Message) {
				return;
			}

			var message = update.Message;
			var command = commandService.Commands.FirstOrDefault(cmd => cmd.Contains(message.Text));
			if (command != null) {
				await command.Execute(botService, message);
			}
		}
	}
}