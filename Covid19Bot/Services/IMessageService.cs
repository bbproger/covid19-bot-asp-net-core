using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace Covid19Bot.Services {
	public interface IMessageService {
		Task ProcessAsync(Update update);
	}
}