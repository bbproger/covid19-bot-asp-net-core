using Telegram.Bot;

namespace Covid19Bot.Services {
	public interface IBotService {
		TelegramBotClient Client { get; }
	}
}