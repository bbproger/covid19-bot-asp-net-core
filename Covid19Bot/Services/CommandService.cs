using System.Collections.Generic;
using Covid19Bot.Commands;
using Microsoft.Extensions.Options;

namespace Covid19Bot.Services {
	public class CommandService : ICommandService {
		private BotConfiguration _config;
		public IEnumerable<Command> Commands { get; private set; }

		public CommandService(IOptions<BotConfiguration> config) {
			_config = config.Value;
			initCommands();
		}

		private void initCommands() {
			var commands = new List<Command>();
			commands.Add(new HelloCommand());
			commands.Add(new LatestCommand(_config.ExternalAPIUrl));
			commands.Add(new CountryCommand(_config.ExternalAPIUrl));
			Commands = commands;
		}
	}
}