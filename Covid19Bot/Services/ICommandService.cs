using System.Collections.Generic;
using Covid19Bot.Commands;

namespace Covid19Bot.Services {
	public interface ICommandService {
		IEnumerable<Command> Commands { get; }
	}
}